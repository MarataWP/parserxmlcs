﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserXML
{
    class Program
    {
        public static string allText;


        static void Main(string[] args)
        {
            string path = "C:/Users/marata/Desktop/Projet C#/ParserXML/res/3_errors.xml";

            string[] allLines = System.IO.File.ReadAllLines(path);
            //string outputError = "";  //should be the error output we fill when find a problem

            //Regex regexFirstLine = new Regex("^<?xml version=\"1.0+\" encoding=\".+\"?>");
            Regex regexFirstLine = new Regex("^<\\?xml version=\"[0-9]+\\.[0-9]+\" encoding=\".+\"\\?>");

            Console.WriteLine(allLines[0]);
            if (regexFirstLine.IsMatch(allLines[0])){
                Console.WriteLine("good first line");
            }
            else
            {
                Console.WriteLine("1st line error");
            }


            allText = "";

            //On regroupe les lignes sans la premiere (deja traitée)
            for (int i = 1; i < allLines.Length; i++)
                allText += allLines[i];

            List<string> openDiv = new List<string>();  //Liste des "div" dont on attend la fermeture.


            string getDivName;
            bool isError;
            bool isNewDiv;
            //on cherche tous les "<"
            for (int i = 0 ; i < allText.Length; i++)
            {
                if(allText[i] == '<')
                {
                    getDivName = findNearestEndOfDiv(i, out isNewDiv, out isError);
                    if (isError)
                    {
                        Console.Write("Error at : ");
                        for(int tmp = i; tmp < tmp+20; tmp++)
                        {
                            if(tmp < allText.Length)
                                Console.Write(allText[tmp]);
                        }
                        return;
                    }

                    if (isNewDiv)
                    {
                        if (openDiv.Contains(getDivName))
                        {
                            Console.WriteLine(getDivName + " est ouverte dans sa propre balise");
                            break;
                        }

                        openDiv.Add(getDivName);
                    }
                    else
                    {
                        if(openDiv.Last() == getDivName)
                        {
                            openDiv.Remove(getDivName);
                        }
                    }


                }
            }

            if(openDiv.Count != 0)
            {
                foreach(string e in openDiv)
                {
                    Console.WriteLine("Balise '" + e + "' pas fermee.");
                }
            }
            else
            {
                Console.WriteLine("XML CORRECT");
            }



            

            Console.ReadKey();
        }



        public static string findNearestEndOfDiv(int index, out bool isNewDiv, out bool isError/*, out int nextIndex*/)
        {
            int indexToStart;
            isError = false;

            if (allText[index + 1] != '/') {
                isNewDiv = true;
                indexToStart = index + 1;
            }
            else {
                isNewDiv = false;
                indexToStart = index + 2;
            }

            string nameOfDiv = "";
            int i;
            for (i = indexToStart; i < allText.Length; i++)
            {
                if(allText[i] == '>')
                {
                    break;
                }
                else
                {
                    nameOfDiv += allText[i];
                }
            }

            if(i == allText.Length)
            {
                isError = true;
            }
            return nameOfDiv;
        }

    }
}
